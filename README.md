###Site Review Helper tool ###

* This is the repository for the Site Review Helper tool developed using .NET
* It was developed using Visual Studio 2012
* The main library used is [**Microsoft Office Interop**
](https://msdn.microsoft.com/en-us/library/microsoft.office.interop.word.aspx)
### Important info : ###

* Configuration XML files are located on a local server which is located in Covent Garden. The tool reads these files using HTTP requests and populates the UI based on them.
* The last version of this tool can be found on Xoomwork's Google Drive [here](https://drive.google.com/a/xoomworks.com/file/d/0B6v9BfonrQIiaU5KME9aeUVLY28/view)

### Who do I talk to? ###

* For information about this you can contact me (John)