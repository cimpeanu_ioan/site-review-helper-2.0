﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace UltimateSiteReviewHelper
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.customTabControl = new System.Windows.Forms.TabControl();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxZendeskCase = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonChangeLaunchReq = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.button_b2c = new System.Windows.Forms.Button();
            this.button_b2b = new System.Windows.Forms.Button();
            this.button_hotelcollect_b2c = new System.Windows.Forms.Button();
            this.button_hotelcollect_b2b = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // customTabControl
            // 
            this.customTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customTabControl.Location = new System.Drawing.Point(16, 90);
            this.customTabControl.Margin = new System.Windows.Forms.Padding(4);
            this.customTabControl.Name = "customTabControl";
            this.customTabControl.SelectedIndex = 0;
            this.customTabControl.Size = new System.Drawing.Size(1180, 539);
            this.customTabControl.TabIndex = 0;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnGenerate.Location = new System.Drawing.Point(16, 648);
            this.btnGenerate.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(179, 28);
            this.btnGenerate.TabIndex = 1;
            this.btnGenerate.Text = "Generate Site Review";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(132, 22);
            this.textBox1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 28);
            this.label2.TabIndex = 0;
            // 
            // textBoxZendeskCase
            // 
            this.textBoxZendeskCase.Location = new System.Drawing.Point(231, 17);
            this.textBoxZendeskCase.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxZendeskCase.MaxLength = 6;
            this.textBoxZendeskCase.Name = "textBoxZendeskCase";
            this.textBoxZendeskCase.Size = new System.Drawing.Size(132, 22);
            this.textBoxZendeskCase.TabIndex = 2;
            this.textBoxZendeskCase.TextChanged += new System.EventHandler(this.textBoxZendeskCase_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(27, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Zendesk Case number:";
            // 
            // buttonChangeLaunchReq
            // 
            this.buttonChangeLaunchReq.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonChangeLaunchReq.Location = new System.Drawing.Point(263, 640);
            this.buttonChangeLaunchReq.Margin = new System.Windows.Forms.Padding(4);
            this.buttonChangeLaunchReq.Name = "buttonChangeLaunchReq";
            this.buttonChangeLaunchReq.Size = new System.Drawing.Size(164, 42);
            this.buttonChangeLaunchReq.TabIndex = 6;
            this.buttonChangeLaunchReq.Text = "Load local Launch Requirements";
            this.buttonChangeLaunchReq.Click += new System.EventHandler(this.buttonChangeLaunchReq_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.labelTitle.ForeColor = System.Drawing.Color.DarkCyan;
            this.labelTitle.Location = new System.Drawing.Point(427, 14);
            this.labelTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(0, 29);
            this.labelTitle.TabIndex = 5;
            // 
            // button_b2c
            // 
            this.button_b2c.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_b2c.Location = new System.Drawing.Point(572, 640);
            this.button_b2c.Margin = new System.Windows.Forms.Padding(4);
            this.button_b2c.Name = "button_b2c";
            this.button_b2c.Size = new System.Drawing.Size(101, 42);
            this.button_b2c.TabIndex = 7;
            this.button_b2c.Text = "B2C";
            this.button_b2c.UseVisualStyleBackColor = true;
            this.button_b2c.Click += new System.EventHandler(this.button_b2c_Click);
            // 
            // button_b2b
            // 
            this.button_b2b.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_b2b.Location = new System.Drawing.Point(693, 639);
            this.button_b2b.Margin = new System.Windows.Forms.Padding(4);
            this.button_b2b.Name = "button_b2b";
            this.button_b2b.Size = new System.Drawing.Size(101, 42);
            this.button_b2b.TabIndex = 8;
            this.button_b2b.Text = "B2B";
            this.button_b2b.UseVisualStyleBackColor = true;
            this.button_b2b.Click += new System.EventHandler(this.button_b2b_Click);
            // 
            // button_hotelcollect_b2c
            // 
            this.button_hotelcollect_b2c.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_hotelcollect_b2c.Location = new System.Drawing.Point(849, 639);
            this.button_hotelcollect_b2c.Margin = new System.Windows.Forms.Padding(4);
            this.button_hotelcollect_b2c.Name = "button_hotelcollect_b2c";
            this.button_hotelcollect_b2c.Size = new System.Drawing.Size(100, 42);
            this.button_hotelcollect_b2c.TabIndex = 9;
            this.button_hotelcollect_b2c.Text = "HotelCollect - B2C";
            this.button_hotelcollect_b2c.UseVisualStyleBackColor = true;
            this.button_hotelcollect_b2c.Click += new System.EventHandler(this.button_hotelcollect_b2c_Click);
            // 
            // button_hotelcollect_b2b
            // 
            this.button_hotelcollect_b2b.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_hotelcollect_b2b.Location = new System.Drawing.Point(971, 639);
            this.button_hotelcollect_b2b.Margin = new System.Windows.Forms.Padding(4);
            this.button_hotelcollect_b2b.Name = "button_hotelcollect_b2b";
            this.button_hotelcollect_b2b.Size = new System.Drawing.Size(100, 42);
            this.button_hotelcollect_b2b.TabIndex = 10;
            this.button_hotelcollect_b2b.Text = "HotelCollect - B2B";
            this.button_hotelcollect_b2b.UseVisualStyleBackColor = true;
            this.button_hotelcollect_b2b.Click += new System.EventHandler(this.button_hotelcollect_b2b_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(928, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(268, 82);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1212, 691);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_hotelcollect_b2b);
            this.Controls.Add(this.button_hotelcollect_b2c);
            this.Controls.Add(this.button_b2b);
            this.Controls.Add(this.button_b2c);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.buttonChangeLaunchReq);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxZendeskCase);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.customTabControl);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainView";
            this.Text = "Awesome Site Review tool";
            this.Load += new System.EventHandler(this.MainView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl customTabControl;
        private Button btnGenerate;
        private TextBox textBox1;
        private Label label2;
        private TextBox textBoxZendeskCase;
        private Label label1;
        private Button buttonChangeLaunchReq;
        private Label labelTitle;
        private Button button_b2c;
        private Button button_b2b;
        private Button button_hotelcollect_b2c;
        private Button button_hotelcollect_b2b;
        private PictureBox pictureBox1;
    }
}

