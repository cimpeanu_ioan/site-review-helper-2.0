﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using UltimateSiteReviewHelper.Models;
using UltimateSiteReviewHelper.Utils;
using UltimateSiteReviewHelper.Views;


namespace UltimateSiteReviewHelper
{
    public partial class MainView : Form
    {
        private RequestView _requestView;
        private List<RequirementModel> _requirements;
        private List<ChapterModel> _chapters;       

        public MainView()
        {
            InitializeComponent();        

            if (TestInternetConnection.TestRequirementAvailability(GlobalValues.B2C) == true || TestInternetConnection.TestRequirementAvailability(GlobalValues.B2C2) == true)
            {
                if (TestInternetConnection.TestRequirementAvailability(GlobalValues.B2C) == true)
                {

                    _requirements = new List<RequirementModel>();
                    _chapters = new List<ChapterModel>();

                    GenerateTabs(GlobalValues.B2C);

                    _requestView = new RequestView();
                    _requestView.Show();
                }
                else
                {
                    _requirements = new List<RequirementModel>();
                    _chapters = new List<ChapterModel>();

                    GenerateTabs(GlobalValues.B2C2);

                    _requestView = new RequestView();
                    _requestView.Show();
                }

            }
            else{
                MessageBox.Show("Please check your internet connection or contact the System Administrator. Please load the requirements file from your local machine.");
                SettingsFileReader.GetNewSettings();
                _requirements = new List<RequirementModel>();
                _chapters = new List<ChapterModel>();
                GenerateTabs();

                _requestView = new RequestView();
                _requestView.Show();
            }            
                   
        }

        #region Generate UI

        public void GenerateTabs(string requirements)

        {
            try
            {
                
                var settings = SettingsFileReader.GetSettings(requirements);
                

                foreach (var tab in settings.Tabs)
                {
                    TabPage tabPage = new TabPage(tab.Name);
                    tabPage.BackColor = Color.White;
                    tabPage.AutoScroll = true;
                    GenerateCheckboxes(tabPage, tab.Requirements);
                    this.customTabControl.TabPages.Add(tabPage);
                }

                this.labelTitle.Text = settings.Title;              


                _chapters = settings.Chapters;
            }
            catch (Exception ex)
            {
                MessageBox.Show("You have to choose a settings file\n");
                Environment.Exit(0);
            }
        }

        public void GenerateTabs()
        {
            try
            {
                var settings = SettingsFileReader.GetSettings();

                foreach (var tab in settings.Tabs)
                {
                    TabPage tabPage = new TabPage(tab.Name);
                    tabPage.BackColor = Color.White;
                    tabPage.AutoScroll = true;
                    GenerateCheckboxes(tabPage, tab.Requirements);

                    this.customTabControl.TabPages.Add(tabPage);
                }

                this.labelTitle.Text = settings.Title;
                _chapters = settings.Chapters;
            }
            catch (Exception ex)
            {
                MessageBox.Show("You have to choose a settings file\n");
                Environment.Exit(0);
            }
        }

      

        private void GenerateCheckboxes(TabPage tabPage, List<RequirementModel> requirements)
        {
            int yPoz = 25;

            foreach (var requirement in requirements)
            {
                var checkBox = new CheckBox();

                var bindingDetails = new Binding("Text", requirement, "Details");
                var bindingIsValid = new Binding("Checked", requirement, "IsValid");

                checkBox.Location = new Point(25, yPoz);
                
                checkBox.DataBindings.Add(bindingDetails);
                checkBox.DataBindings.Add(bindingIsValid);
                //checkBox.Size = new Size((int)checkBox.CreateGraphics().MeasureString(requirement.Details, checkBox.Font).Width, 20);
                checkBox.Size = new System.Drawing.Size(600, 20);
               
                tabPage.Controls.Add(checkBox);
                
                _requirements.Add(requirement);

                yPoz += 25;

            }

            GenerateAdditionalRichTextBox(tabPage, yPoz + 60);
        }

        private void GenerateAdditionalRichTextBox(TabPage tabPage, int yPoz)
        {
            var additionalRequirement = new RequirementModel
            {
                Details = "Additional",
                Cref = "0",
                IsValid = false,
                Resolution = ""
            };

            var checkBox = new CheckBox();
            var richTextBox = new RichTextBox();

            var bindingDetails = new Binding("Text", additionalRequirement, "Details");
            var bindingIsValid = new Binding("Checked", additionalRequirement, "IsValid");
            var bindingResolution = new Binding("Text", additionalRequirement, "Resolution");

            checkBox.Location = new Point(20, yPoz);
            checkBox.DataBindings.Add(bindingDetails);
            checkBox.DataBindings.Add(bindingIsValid);

            yPoz += 30;

            richTextBox.Location = new Point(20, yPoz);
            richTextBox.Size = new Size(750, 100);
            richTextBox.DataBindings.Add(bindingResolution);

            tabPage.Controls.Add(checkBox);
            tabPage.Controls.Add(richTextBox);

            _requirements.Add(additionalRequirement);
        }

        #endregion

        #region Event Handlers

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            WordFileGenerator.GenerateSiteReview(_requestView.GetPartnerInformation(),
                _requirements, _chapters);

        }

        private void buttonChangeLaunchReq_Click(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show("Note that any unsaved work will be lost. Do you want to proceed? ", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (dialogResult == DialogResult.Yes)
            {
                customTabControl.TabPages.Clear();
                SettingsFileReader.GetNewSettings();
                _requirements.Clear();
                _chapters.Clear();

                GenerateTabs();
            }
        }
        #endregion

        //Global variable class

        private void textBoxZendeskCase_TextChanged(object sender, EventArgs e)
        {

            var zdNumberString = textBoxZendeskCase.Text;

            if (Regex.IsMatch(zdNumberString, @"^[0-9]+$"))
            {
                GlobalValue.UserWidth = textBoxZendeskCase.Text;
            }
            else
            {
                textBoxZendeskCase.Text = "";
                GlobalValue.UserWidth = "";
            }
            
        }

        private void button_b2c_Click(object sender, EventArgs e)
        {
            if (TestInternetConnection.TestRequirementAvailability(GlobalValues.B2C) == true || TestInternetConnection.TestRequirementAvailability(GlobalValues.B2C2) == true)
            {
                var dialogResult = MessageBox.Show("Note that any unsaved work will be lost. Do you want to proceed? ", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dialogResult == DialogResult.Yes)
                {
                    if (TestInternetConnection.TestRequirementAvailability(GlobalValues.B2C) == true)
                    {
                        customTabControl.TabPages.Clear();
                        SettingsFileReader.GetSettings(GlobalValues.B2C);
                        _requirements.Clear();
                        _chapters.Clear();
                        GenerateTabs(GlobalValues.B2C);
                    }
                    else
                    {
                        customTabControl.TabPages.Clear();
                        SettingsFileReader.GetSettings(GlobalValues.B2C2);
                        _requirements.Clear();
                        _chapters.Clear();
                        GenerateTabs(GlobalValues.B2C2);
                    }
                }               
            }
            else {
                MessageBox.Show("The requirements cannot be retrieved from the server. Please load local files.");           
            }
        }


        private void button_b2b_Click(object sender, EventArgs e)
        {
            if (TestInternetConnection.TestRequirementAvailability(GlobalValues.B2B) == true || TestInternetConnection.TestRequirementAvailability(GlobalValues.B2B2) == true)
            {
                var dialogResult = MessageBox.Show("Note that any unsaved work will be lost. Do you want to proceed? ", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dialogResult == DialogResult.Yes)
                {

                    if (TestInternetConnection.TestRequirementAvailability(GlobalValues.B2B) == true)
                    {
                        customTabControl.TabPages.Clear();
                        SettingsFileReader.GetSettings(GlobalValues.B2B);
                        _requirements.Clear();
                        _chapters.Clear();
                        GenerateTabs(GlobalValues.B2B);
                    }
                    else
                    {
                        customTabControl.TabPages.Clear();
                        SettingsFileReader.GetSettings(GlobalValues.B2B2);
                        _requirements.Clear();
                        _chapters.Clear();
                        GenerateTabs(GlobalValues.B2B2);
                    }
                }
            }
            else
            {
                MessageBox.Show("The requirements cannot be retrieved from the server. Please load local files.");
            }
        }

        

        private void button_hotelcollect_b2c_Click(object sender, EventArgs e)
        {
            if (TestInternetConnection.TestRequirementAvailability(GlobalValues.HC_B2C) == true || TestInternetConnection.TestRequirementAvailability(GlobalValues.HC_B2C2) == true)
            {
                var dialogResult = MessageBox.Show("Note that any unsaved work will be lost. Do you want to proceed? ", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dialogResult == DialogResult.Yes)
                {

                    if (TestInternetConnection.TestRequirementAvailability(GlobalValues.HC_B2C) == true)
                    {
                        customTabControl.TabPages.Clear();
                        SettingsFileReader.GetSettings(GlobalValues.HC_B2C);
                        _requirements.Clear();
                        _chapters.Clear();
                        GenerateTabs(GlobalValues.HC_B2C);
                    }
                    else
                    {
                        customTabControl.TabPages.Clear();
                        SettingsFileReader.GetSettings(GlobalValues.HC_B2C2);
                        _requirements.Clear();
                        _chapters.Clear();
                        GenerateTabs(GlobalValues.HC_B2C2);
                    }
                }
            }
            else
            {
                MessageBox.Show("The requirements cannot be retrieved from the server. Please load local files.");
            }
        }

        private void button_hotelcollect_b2b_Click(object sender, EventArgs e)
        {
            if (TestInternetConnection.TestRequirementAvailability(GlobalValues.HC_B2B) == true || TestInternetConnection.TestRequirementAvailability(GlobalValues.HC_B2B2) == true)
            {
                var dialogResult = MessageBox.Show("Note that any unsaved work will be lost. Do you want to proceed? ", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (dialogResult == DialogResult.Yes)
                {

                    if (TestInternetConnection.TestRequirementAvailability(GlobalValues.HC_B2B) == true)
                    {
                        customTabControl.TabPages.Clear();
                        SettingsFileReader.GetSettings(GlobalValues.HC_B2B);
                        _requirements.Clear();
                        _chapters.Clear();
                        GenerateTabs(GlobalValues.HC_B2B);
                    }
                    else
                    {
                        customTabControl.TabPages.Clear();
                        SettingsFileReader.GetSettings(GlobalValues.HC_B2B2);
                        _requirements.Clear();
                        _chapters.Clear();
                        GenerateTabs(GlobalValues.HC_B2B2);
                    }
                }
            }
            else
            {
                MessageBox.Show("The requirements cannot be retrieved from the server. Please load local files.");
            }
        }

        private void MainView_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

       

      

    }
}

    


