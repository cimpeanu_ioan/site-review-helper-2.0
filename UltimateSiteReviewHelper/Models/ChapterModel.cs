﻿using System.Xml.Serialization;
namespace UltimateSiteReviewHelper.Models
{
    public class ChapterModel
    {
        [XmlText()]
        public string Name { get; set; }

        [XmlAttribute("id")]
        public string Id { get; set; }
    }
}
