﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.ComponentModel;

namespace UltimateSiteReviewHelper.Models
{
    public class RequirementModel
    {
        [XmlAttribute("details")]
        public string Details { get; set; }

        [XmlAttribute("cref")]
        public string Cref { get; set; } 

        [XmlElement("resolution")]
        public string Resolution { get; set; }

        [XmlElement("hyperlink")]
        public string Hyperlynk { get; set; }

        [DefaultValue(false)]
        public bool IsValid { get; set; }
    }
}
