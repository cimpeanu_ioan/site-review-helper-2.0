﻿using System.Collections.Generic;
using System.Xml.Serialization;


namespace UltimateSiteReviewHelper.Models
{
    [XmlRoot("root")]
    public class SettingsModel
    {
        [XmlElement("tab")]
        public List<TabModel> Tabs { get; set; }

        [XmlElement("chapter")]
        public List<ChapterModel> Chapters { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

       
        
    }
}
