﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace UltimateSiteReviewHelper.Models
{
    public class TabModel
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("requirement")]
        public List<RequirementModel> Requirements { get; set; }
    }
}
