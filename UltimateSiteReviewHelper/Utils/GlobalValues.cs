﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UltimateSiteReviewHelper.Utils
{
    public static class GlobalValues
    {
        //Locations of the config files + backups

        public static string B2C = "http://192.168.0.204/site_review_config/B2C.xml";
        public static string B2C2 = "http://195.50.71.47/sitereview/B2C.xml";

        public static string B2B = "http://192.168.0.204/site_review_config/B2B.xml";
        public static string B2B2 = "http://195.50.71.47/sitereview/B2B.xml";

        public static string HC_B2B = "http://192.168.0.204/site_review_config/HC_B2B.xml";
        public static string HC_B2B2 = "http://195.50.71.47/sitereview/HC_B2B.xml";

        public static string HC_B2C = "http://192.168.0.204/site_review_config/HC_B2C.xml";
        public static string HC_B2C2 = "http://195.50.71.47/sitereview/HC_B2C.xml";
        
    }


}
