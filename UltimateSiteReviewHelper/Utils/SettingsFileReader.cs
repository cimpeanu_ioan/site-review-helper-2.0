﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using UltimateSiteReviewHelper.Models;
using UltimateSiteReviewHelper.Properties;
namespace UltimateSiteReviewHelper.Utils
{
    public static class SettingsFileReader
    {
       public static string _settingsLocation;

        public static SettingsModel GetSettings(string requirements)
        {
            _settingsLocation = requirements;

            SettingsModel settingsModel = null;
            if (String.IsNullOrEmpty(_settingsLocation))
            {
                _settingsLocation = Resources.SettingsLocation;
            }

            var fileExists = File.Exists(_settingsLocation);
            XmlReader reader = null;
            while (!fileExists)
            {

                //_settingsLocation = BrowseForSettingsFile();
                //string fileURL = "http://xpedia.xoomworks.ro/tools/site_review_helper_conf/B2C.xml";
                //XmlDocument myXmlDocument = new XmlDocument();
                //myXmlDocument.LoadXml(fileURL);

                //StringWriter sw = new StringWriter();
                //XmlTextWriter tx = new XmlTextWriter(sw);
                //myXmlDocument.WriteTo(tx);
                //_settingsLocation = requirements;
                

                if (_settingsLocation == null)
                    throw new Exception(Resources.SettingsFileMissing);

                XmlDocument myXmlDocument = new XmlDocument();
                myXmlDocument.XmlResolver = null;
                XmlUrlResolver resolver = new XmlUrlResolver();
                resolver.Credentials = new NetworkCredential("xoomworks", "oviiscool");

                myXmlDocument.XmlResolver = resolver;
                myXmlDocument.Load(_settingsLocation);
                reader = XmlReader.Create(new StringReader(myXmlDocument.InnerXml));

               


                fileExists = File.Exists(_settingsLocation);
                fileExists = true;
            }

            try
            {
                var serializer = new XmlSerializer(typeof(SettingsModel));
                
                    settingsModel = (SettingsModel)serializer.Deserialize(reader);
                    reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (settingsModel == null)
                throw new Exception("Parsing error: The settings model is null.");

            if (settingsModel.Tabs == null)
                throw new Exception("Parsing error: The tabs list is null.");

            return settingsModel;
        }

        public static SettingsModel GetSettings()
        {

            SettingsModel settingsModel = null;
            if (String.IsNullOrEmpty(_settingsLocation))
            {
                _settingsLocation = Resources.SettingsLocation;
            }

            var fileExists = File.Exists(_settingsLocation);
            while (!fileExists)
            {

                _settingsLocation = BrowseForSettingsFile();
                if (_settingsLocation == null)
                    throw new Exception(Resources.SettingsFileMissing);

                fileExists = File.Exists(_settingsLocation);
            }

            try
            {
                var serializer = new XmlSerializer(typeof(SettingsModel));


                using (TextReader reader = new StreamReader(_settingsLocation))
                {
                    settingsModel = serializer.Deserialize(reader) as SettingsModel;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (settingsModel == null)
                throw new Exception("Parsing error: The settings model is null.");

            if (settingsModel.Tabs == null)
                throw new Exception("Parsing error: The tabs list is null.");

            return settingsModel;
        }
        //Get new settings

        public static SettingsModel GetNewSettings()
        {

            SettingsModel settingsModel = null;
            if (String.IsNullOrEmpty(_settingsLocation))
            {
                _settingsLocation = Resources.SettingsLocation;
            }
            _settingsLocation = null;
            var fileExists = File.Exists(_settingsLocation);
            while (!fileExists)
            {
                // MessageBox.Show("You will now load different Launch Requirements. Note that any unsaved work will be lost!");

                _settingsLocation = BrowseForSettingsFile();


                if (_settingsLocation == null)
                {
                    //throw new Exception(Resources.SettingsFileMissing);    

                    var dialogResult = MessageBox.Show("Please select a valid requirements file. ", "No file selected.", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    if (dialogResult == DialogResult.Yes)
                    {
                        continue;
                    }
                    else
                    {

                        UltimateSiteReviewHelper.MainView initialView = new UltimateSiteReviewHelper.MainView();
                        initialView.GenerateTabs(GlobalValues.B2C);
                    }

                }
                else
                {
                    break;
                }
            }

            try
            {
                var serializer = new XmlSerializer(typeof(SettingsModel));

                using (TextReader reader = new StreamReader(_settingsLocation))
                {
                    settingsModel = serializer.Deserialize(reader) as SettingsModel;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (settingsModel == null)
                throw new Exception("Parsing error: The settings model is null.");

            if (settingsModel.Tabs == null)
                throw new Exception("Parsing error: The tabs list is null.");

            return settingsModel;
        }

        //Browse file method
        public static string BrowseForSettingsFile()
        {
            var openFileDialog = new OpenFileDialog();
            var dialogResult = openFileDialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                return openFileDialog.InitialDirectory + openFileDialog.FileName;
            }
            else
            {
                return _settingsLocation;
            }

          //  return null;
        }

     
    }
}
