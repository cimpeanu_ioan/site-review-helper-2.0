﻿using Microsoft.Office.Interop.Word;
using System.Collections.Generic;
using System.Windows.Forms;
using UltimateSiteReviewHelper.Models;

namespace UltimateSiteReviewHelper.Utils
{
    public static class WordFileGenerator
    {

        public static void GenerateSiteReview(List<string> partnerInfo, List<RequirementModel> requirements, List<ChapterModel> chapters)
        {
            object oMissing = System.Reflection.Missing.Value;
            object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */


            {
                //Get the Gobal variable Zdcasenr

                string Zdcasenr = GlobalValue.UserWidth;
                if (!Zdcasenr.Equals(""))
                {


                    //Start Word and create a new document.

                    Microsoft.Office.Interop.Word._Application oWord;
                    Microsoft.Office.Interop.Word._Document oDoc;
                    oWord = new Microsoft.Office.Interop.Word.Application();
                    oWord.Visible = true;
                    oDoc = oWord.Documents.Add(ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                    //Add Zendesk case number.

                    Microsoft.Office.Interop.Word.Paragraph zendesk;
                    zendesk = oDoc.Content.Paragraphs.Add(ref oMissing);
                    zendesk.Range.Text = "Zendesk Case number:" + Zdcasenr;
                    zendesk.Range.Font.Bold = 1;
                    zendesk.Range.Font.Size = 20;
                    zendesk.Range.Font.ColorIndex = WdColorIndex.wdRed;
                    zendesk.Range.InsertParagraphAfter();

                    //Add Main title
                    Microsoft.Office.Interop.Word.Paragraph mainTitle;
                    mainTitle = oDoc.Content.Paragraphs.Add(ref oMissing);
                    mainTitle.Range.Text = "Site Review";
                    mainTitle.Range.Font.Bold = 1;
                    mainTitle.Format.SpaceAfter = 5;    //24 pt spacing after paragraph.
                    mainTitle.Range.Font.Size = 24;
                    mainTitle.Range.Font.ColorIndex = WdColorIndex.wdBlue;
                    mainTitle.Range.InsertParagraphAfter();


                    string style = "Heading 1";
                    object objStyle = style;


                    // Partner Information section
                    Microsoft.Office.Interop.Word.Paragraph paraPartnerInformation;
                    paraPartnerInformation = oDoc.Content.Paragraphs.Add(ref oMissing);
                    paraPartnerInformation.Range.Text = "Partner information";
                    paraPartnerInformation.Range.Font.Bold = 1;
                    paraPartnerInformation.Format.SpaceAfter = 5;    //24 pt spacing after paragraph.
                    paraPartnerInformation.set_Style(ref objStyle);
                    paraPartnerInformation.Range.Font.Size = 18;
                    paraPartnerInformation.Range.Font.ColorIndex = WdColorIndex.wdBlue;
                    paraPartnerInformation.Range.InsertParagraphAfter();

                    foreach (string info in partnerInfo)
                    {
                        AddItem("• " + info, oDoc, ref oMissing);
                    }

                    // Chapter Section
                    foreach (var chapter in chapters)
                    {
                        AddHeadline(chapter.Name, oDoc, ref oMissing);

                        var chapterRequirementsList = requirements.FindAll(x => x.Cref == chapter.Id);
                        foreach (var chapterRequirement in chapterRequirementsList)
                        {
                            AddSubtitle("• " + chapterRequirement.Details, oDoc, ref oMissing);
                            AddResult(chapterRequirement.IsValid, oDoc, ref oMissing);
                        }
                    }


                    // Issue Section
                    AddHeadline("Issues - summary", oDoc, ref oMissing);

                    foreach (var requirement in requirements)
                    {
                        if (requirement.Cref == "0")
                            requirement.IsValid = !requirement.IsValid;

                        if (!requirement.IsValid)
                        {
                            foreach (var issue in requirement.Resolution.Split('\n'))
                            {
                                AddHyperlink("• " + issue, oDoc, ref oMissing, requirement.Hyperlynk);
                            }
                        }
                    }


                    MessageBox.Show("Site Review successfuly generated !");
                    MessageBox.Show("!!! Do not forget to attach the document to the Zendesk Case !!!");
                }
                else
                    MessageBox.Show("Please provide the Zendesk case number! ");



            }

        }




        #region Private Methods

        private static void AddItem(string issue, Microsoft.Office.Interop.Word._Document oDoc, ref object oMissing)
        {
            Microsoft.Office.Interop.Word.Paragraph paraPartnerDetail;
            paraPartnerDetail = oDoc.Content.Paragraphs.Add(ref oMissing);
            paraPartnerDetail.Range.Text = issue;
            paraPartnerDetail.Range.Font.Bold = 0;
            paraPartnerDetail.Format.SpaceAfter = 3;    //24 pt spacing after paragraph.
            paraPartnerDetail.Format.LeftIndent = 15.0f;
            paraPartnerDetail.Range.Font.Size = 14;
            paraPartnerDetail.Range.Font.ColorIndex = WdColorIndex.wdBlack;

            paraPartnerDetail.Range.InsertParagraphAfter();
        }

        private static void AddHyperlink(string issue, Microsoft.Office.Interop.Word._Document oDoc, ref object oMissing, string hyperlink)
        {
            bool isNull = true;
            Microsoft.Office.Interop.Word.Paragraph paraPartnerDetail;
            paraPartnerDetail = oDoc.Content.Paragraphs.Add(ref oMissing);
            paraPartnerDetail.Range.Text = issue;
            paraPartnerDetail.Range.Font.Bold = 0;
            paraPartnerDetail.Format.SpaceAfter = 3;    //24 pt spacing after paragraph.
            paraPartnerDetail.Format.LeftIndent = 15.0f;
            object url = hyperlink;
            if (url == "" || url == null)
            {
                isNull = true;
            }
            else
            {
                isNull = false;
            }
            switch (isNull)
            {
                case true:
                    break;
                case false:
                    paraPartnerDetail.Range.Hyperlinks.Add(paraPartnerDetail.Range, ref url);
                    break;
            }

            paraPartnerDetail.Range.Font.Size = 14;
            paraPartnerDetail.Range.InsertParagraphAfter();
        }

        private static void AddHeadline(string text, Microsoft.Office.Interop.Word._Document oDoc, ref object oMissing)
        {
            string style = "Heading 1";
            object objStyle = style;

            Microsoft.Office.Interop.Word.Paragraph clear;
            clear = oDoc.Content.Paragraphs.Add(ref oMissing);
            clear.Range.InsertBreak();

            Microsoft.Office.Interop.Word.Paragraph headline;
            headline = oDoc.Content.Paragraphs.Add(ref oMissing);
            headline.Range.Text = text;
            headline.Range.Font.Bold = 1;
            headline.Format.SpaceAfter = 5;    //24 pt spacing after paragraph.
            headline.Format.LeftIndent = 0.0f;
            headline.set_Style(objStyle);
            headline.Range.Font.Size = 18;
            headline.Range.Font.ColorIndex = WdColorIndex.wdBlue;

            headline.Range.InsertParagraphAfter();
        }

        private static void AddSubtitle(string text, Microsoft.Office.Interop.Word._Document oDoc, ref object oMissing)
        {
            Microsoft.Office.Interop.Word.Paragraph headline;
            headline = oDoc.Content.Paragraphs.Add(ref oMissing);
            headline.Range.Text = text;
            headline.Range.Font.Bold = 0;
            headline.Format.SpaceAfter = 3;    //24 pt spacing after paragraph.
            headline.Format.LeftIndent = 15.0f;

            headline.Range.Font.Size = 12;
            headline.Range.Font.ColorIndex = WdColorIndex.wdBlack;

            headline.Range.InsertParagraphAfter();
        }

        private static void AddResult(bool result, Microsoft.Office.Interop.Word._Document oDoc, ref object oMissing)
        {

            if (result)
            {
                Microsoft.Office.Interop.Word.Paragraph pass;
                pass = oDoc.Content.Paragraphs.Add(ref oMissing);
                pass.Range.Text = "PASS";
                pass.Range.Font.Bold = 1;

                pass.Range.Font.Size = 14;
                pass.Range.Font.ColorIndex = WdColorIndex.wdGreen;

                pass.Range.InsertParagraphAfter();
            }
            else
            {
                Microsoft.Office.Interop.Word.Paragraph pass;
                pass = oDoc.Content.Paragraphs.Add(ref oMissing);
                pass.Range.Text = "UPDATE REQUIRED";
                pass.Range.Font.Bold = 1;

                pass.Range.Font.Size = 14;
                pass.Range.Font.ColorIndex = WdColorIndex.wdRed;

                pass.Range.InsertParagraphAfter();
            }
        }
    }


        #endregion
}



