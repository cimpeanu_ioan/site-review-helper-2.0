﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UltimateSiteReviewHelper.Views
{
    public partial class RequestView : Form
    {
        public RequestView()
        {
            InitializeComponent();
        }

        public List<string> GetPartnerInformation()
        {
            List<string> result = new List<string>();

            string rawInfo = richTextBoxPartnerInfo.Text;

            string[] rawInfoList = rawInfo.Split('\n');

            foreach (string line in rawInfoList)
            {
                string[] elements = line.Split(' ');

                try
                {
                    if (elements[0] == "Affiliate" && elements[1] == "Username:")
                    {
                        string newElem = "Username: " + elements[2];

                        for (int i = 3; i < elements.Length; i++)
                            newElem += " " + elements[i];

                        result.Add(newElem);
                    }
                }
                catch { }

                try
                {
                    if (elements[0] == "Affiliate" && elements[1] == "email" && elements[2] == "address:")
                    {
                        string newElem = "E-mail: " + elements[3];

                        for (int i = 4; i < elements.Length; i++)
                            newElem += " " + elements[i];

                        result.Add(newElem);
                    }
                }
                catch { }

                try
                {
                    if (elements[0] == "API" && elements[1] == "key:")
                    {
                        string newElem = "API Key: " + elements[2];

                        for (int i = 3; i < elements.Length; i++)
                            newElem += " " + elements[i];

                        result.Add(newElem);
                    }
                }
                catch { }

                try
                {
                    if (elements[0] == "Application" && elements[1] == "Name:")
                    {
                        string newElem = "Name: " + elements[2];

                        for (int i = 3; i < elements.Length; i++)
                            newElem += " " + elements[i];

                        result.Add(newElem);
                    }
                }
                catch { }

                try
                {
                    if (elements[0] == "CID:")
                    {
                        string newElem = "CID: " + elements[1];

                        for (int i = 2; i < elements.Length; i++)
                            newElem += " " + elements[i];

                        result.Add(newElem);
                    }
                }
                catch { }

                try
                {
                    if (elements[0] == "Integration" && elements[1] == "Type:")
                    {
                        string newElem = "Integration Type: " + elements[2];

                        for (int i = 3; i < elements.Length; i++)
                            newElem += " " + elements[i];

                        result.Add(newElem);
                    }
                }
                catch { }

                try
                {
                    if (elements[0] == "Web" && elements[1] == "site:")
                    {
                        string newElem = "URL: " + elements[2];

                        for (int i = 3; i < elements.Length; i++)
                            newElem += " " + elements[i];

                        result.Add(newElem);
                    }
                }
                catch { }

                try
                {
                    if (elements[0] == "Hotel" && elements[1] == "integration:")
                    {
                        string newElem = "•	Hotel integration: " + elements[2];

                        for (int i = 3; i < elements.Length; i++)
                            newElem += " " + elements[i];

                        result.Add(newElem);
                    }
                }
                catch { }
            }

            string reviewDate = "Review date and time: ";
            reviewDate += DateTime.Today.Day.ToString() + "." + DateTime.Today.Month.ToString() + "." + DateTime.Today.Year.ToString();

            result.Add(reviewDate);

            return result;
        }

    }
}
